def palabras_usuario(a):
    stop = 0
    while stop == 0:
        words = str(input("Ingrese una palabra en español e inglés separada por un ; de la siguiente manera: palabra ; traducción\n"))
        traducir = words.split(" ; ")
        seguir = str(input("¿Desea ingresar otra palabra? s/n: \n"))
        a[traducir[0]] = traducir[1]
        if seguir == 'n':
            break

def traduccion_frase(b):
    x = 0
    frase = str(input("\nIngrese su frase a traducir: "))
    palabras = frase.split()
    for n in palabras:
        for key in b:
            if key == palabras[x]:
                palabras[x] = b[key]       
        x = x + 1
    phrase = " ".join(palabras)
    print("Su frase traducida es: ",phrase)

dict = {}
palabras_usuario(dict)
traduccion_frase(dict)
