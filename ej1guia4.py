def menu(dict): # menú
    reinicio = 1
    while reinicio != 0:
        print("¡Bienvenido a su diccionario de proteínas! ¿Qué desea hacer?\n")
        n = str(input("Agregar Elemento = a ; Editar Elemento = b ; Consultar Key = c ; Eliminar Elemento = d ; Imprimir Diccionario = e: "))
        if n == "a":
            dict = agregar(dict)
        elif n == "b":
            dict = editar(dict)
        elif n == "c":
            dict = consultar(dict)
        elif n == "d":
            dict = eliminar(dict)
        elif n == "e":
            dict = imprimir(dict)
        else:
            print("\nError: Ingrese una tecla válido: ")
        reinicio = str(input("\nPresione r para volver a consultar o s para salir: "))
        print("\n")
        if reinicio == "s":
            break
    return dict

def agregar(a):
    newkey = str(input("\nIngrese la nueva key: "))
    newvalue = str(input("Ingrese el nuevo valor: "))
    dict.update ({ newkey : newvalue }) # agrega elemento al dict
    for key in dict:
        print(key, ":", dict[key])
    return a

def editar(b):
    editkey = str(input("\nIngrese la key del valor que desea editar: "))
    editvalue = str(input("Ingrese el nuevo valor: "))
    dict[editkey] = editvalue # imprime el nuevo valor editado
    for key in dict:
        print(key, ":", dict[key])
    return b

def consultar(c):
    key = str(input("\nIngrese la key del valor por el cuál desea consultar: ")) 
    print(dict[key]) # imprime valor mediante key
    return c

def eliminar(d):
    keydel = str(input("\nIngrese la key del valor que desea eliminar: "))
    del dict[keydel] # elimina elemento
    for key in dict:
        print(key, ":", dict[key])
    return d

def imprimir(e):
    print("\n")
    for key in dict: # imprime dict completo
        print(key, ":", dict[key])
    return e

dict = {"6S1A" : "Ligand binding domain of the P. putida receptor PcaY_PP" ,
        "6S18" : "Ligand binding domain of the P. putida receptor PcaY_PP in complex with glycerol" ,
        "6S33" : "Ligand binding domain of the P. putida receptor PcaY_PP in complex with Protocatechuate" ,
        "6S3B" : "Ligand binding domain of the P. putida receptor PcaY_PP in complex with benzoate" ,
        "6S37" : "Ligand binding domain of the P. putida receptor PcaY_PP in complex with salicylic acid" ,
        "6S38" : "Ligand binding domain of the P. putida receptor PcaY_PP in complex with quinate" ,
        "6S87" : "Crystal structure of 2-methylcitrate synthase (PrpC) from Pseudomonas aeruginosa in complex with oxaloacetate" ,
        "6S96" : "Crystal structure of the catalytic domain of UBE2S C118A" ,
        "6S98" : "Crystal structure of the catalytic domain of UBE2S WT"}
menu(dict)